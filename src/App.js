import React, { useEffect, useState } from 'react';
import './App.css';
// import React from 'react';
import axios from 'axios';
let Chuck = "https://e7.pngegg.com/pngimages/260/596/png-clipart-chuck-norris-facts-meme-joke-martial-arts-chuck-norris-celebrities-arm-thumbnail.png"

function App() {

  const [state, setState] = useState({
    joke:''
  })

  useEffect( () => {
    fetchData();
  }, []);
  
  const fetchData = async () => {
    const result = await axios.get('https://api.chucknorris.io/jokes/random')
    console.log(result.data.value); 
    setState({
      ...state, //recupere tout l'objet
      joke: result.data.value
    });
  }

  return (
    <div className="container" >
      <div className="row" >
        <div className="col-6" >
          <h1 className="title">
            Chuck Norris Quotes
          </h1>
          <img src={Chuck} alt="yop" />
        </div>
        <div className="col-6" >
          <div className="card" >
            <div className="card-header" >
              <span>
                search
              </span>
            </div>
            <div className="card-body" >
              <input className="searchOption" type="text" />
            </div>
            <div>
              <button className="btn btn-warning">
                Generate
              </button>
            </div>
          </div>

          <div className="col-12" >

            <div className="card" >
              <h1>
                et la blague :
              </h1>
              <h2>
                {state.joke}
              </h2>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}

export default App;
