import React from "react";
import ReactDOM from "react-dom";
import App from './App.js';
import './App.css';

// const App = () => <h1>Chuck's app</h1>;

const rootElement = document.getElementById("root");

ReactDOM.render(<App />, rootElement);

module.hot.accept();